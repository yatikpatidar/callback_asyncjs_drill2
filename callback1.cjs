//Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.

function getBoardsInfo(boardsData, boardId, callback) {

    setTimeout(() => {
        let list = boardsData.reduce((res, currData) => {
            if (currData['id'] === boardId) {
                res.push(currData)
            }
            return res
        }, [])

        callback(null, list)


    }, 2000)
}

module.exports = getBoardsInfo