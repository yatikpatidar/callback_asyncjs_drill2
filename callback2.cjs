//Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.

function getListInfo(listData, boardID, callback) {
    setTimeout(() => {

        const list = Object.entries(listData).reduce((res, currData) => {

            if (currData[0] === boardID) {
                res= currData[1]
            }
            return res

        }, [])
        callback(null, list)


    }, 2000)
}


module.exports = getListInfo