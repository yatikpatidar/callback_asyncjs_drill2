// Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.

const getBoardInfo = require('./../callback1.cjs')
const boardData = require('./../data/boards_2.json')

getBoardInfo(boardData, "mcu453ed", (err, data) => {
    try {
        if (err) {
            throw new Error(err)
        } else {
            console.log(data)
        }
    }
    catch (err) {
        console.error(err)
    }
})