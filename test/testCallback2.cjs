//Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.

const getListInfo = require('./../callback2.cjs')
const listData = require("./../data/lists_1.json")


getListInfo(listData, "mcu453ed", (err, data) => {
    try {
        if (err) {
            throw new Error(err)
        } else {
            console.log(data)
        }
    }
    catch (err) {
        console.error(err)
    }
})