
/* 
	Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/

const boardData = require('./../data/boards_2.json')
const gettingInformation = require("./../callback6.cjs")
gettingInformation(boardData, "Thanos")