
/* 
	Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/

const getBoardInfo = require('./callback1.cjs')
const getListInfo = require('./callback2.cjs')
const getCardsInfo = require('./callback3.cjs')

const listData = require('./data/lists_1.json')
const cardsData = require('./data/cards_2.json')

function gettingInformation(boardData, nameToFind) {

    try {
        const boardIdOfName = boardData.reduce((res, currData) => {

            if (currData['name'] === nameToFind) {
                res = currData['id']
            }
            return res

        }, "")

        getBoardInfo(boardData, boardIdOfName, (err, data) => {

            if (err) {
                throw new Error(err)
            } else {

                console.log("information for thanos board ", data)
                const thanosId = data[0]['id']

                getListInfo(listData, thanosId, (listErr, listCallbackData) => {

                    if (listErr) {
                        throw new Error(errList)
                    } else {

                        console.log("getting lists of all for Thanos board" , listCallbackData)
                        
                        console.log("Getting all cards for all lists" )
                        
                        listCallbackData.forEach((currKey) => {


                            getCardsInfo(cardsData, currKey['id'], (cardErr, cardCallbackData) => {

                                if (cardErr) {
                                    throw new Error(cardErr)
                                }
                                else {
                                    console.log(cardCallbackData)
                                }
                            })

                        })
                    }
                })
            }
        })

    } catch (err) {
        console.error(err)
    }
}

module.exports = gettingInformation

