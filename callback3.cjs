/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/

function getCardsInfo(cardsData, listID, callback) {
    setTimeout(() => {

        const list = Object.entries(cardsData).reduce((res, currData) => {

            if (currData[0] === listID) {
                return currData[1]
            }
            return res

        }, [])

        if (list.length != 0) {
            callback(null, list)
        }

    }, 2000)
}

module.exports = getCardsInfo